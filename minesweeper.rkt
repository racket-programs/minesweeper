#lang racket
(require rackunit 2htdp/universe 2htdp/image)

(define WIDTH 16)
(define HEIGHT 16)
(define MINECOUT (quotient (* WIDTH HEIGHT) 6))
(define SIZE 35)

; field: mine field cell
(struct field (visible mines) #:transparent)

(define mine-field (field #f 'mine))

(define empty-field (field #f 0))

(define (mine-field? f)
  (eq? 'mine (field-mines f)))

(define (empty-field? f)
  (and (not (mine-field? f)) (zero? (field-mines f))))

(define (field-add1 f)
  (field (field-visible f) (add1 (field-mines f))))

(define empty-fields (make-list (* WIDTH HEIGHT) empty-field))

(define (field-index x y)
  (+ x (* WIDTH y)))

(define (in-field? x y)
  (and (<= 0 x) (> WIDTH x) (<= 0 y) (> HEIGHT y)))

(define (adjacent-field-list x y)
  (filter
   (lambda (pos)
     (and (in-field? (first pos) (second pos)) (not (equal? pos (list x y)))))
  (map (lambda (lst) (list (+ x (first lst)) (+ y (second lst))))
       (cartesian-product '(-1 0 1) '(-1 0 1)))))

(define (update-fields x y lst)
  (foldl
   (lambda (pos lst)
     (let ([idx (field-index (first pos) (second pos))])
       (if (not (mine-field? (list-ref lst idx)))
           (list-set lst idx (field-add1 (list-ref lst idx))) 
           lst)))
   lst
   (adjacent-field-list x y)))
   
(define (random-field num lst)
  (let* ([x (random WIDTH)]
         [y (random HEIGHT)]
         [i (field-index x y)])
    (cond
      [(zero? num) lst]
      [(mine-field? (list-ref lst i)) (random-field num lst)]
      [else (random-field (sub1 num) (update-fields x y (list-set lst i mine-field)))])))
   
(define (enter-field x y lst)
  (let* ([idx (field-index x y)][f (list-ref lst idx)][ls (list-set lst idx (field #t (field-mines f)))])
    (if
     (empty-field? f)
     (foldl
      (lambda (pos lst)
        (let* ([x (first pos)][y (second pos)])
          (if (and (in-field? x y) (not (field-visible (list-ref lst (field-index x y))))) (enter-field x y lst) lst)))
      ls
      (adjacent-field-list x y))
     ls)))

(define (mine-visible? lst)
  (ormap (lambda (f) (and (mine-field? f) (field-visible f))) lst))

(define (game-end? lst)
  (or
   (mine-visible? lst)
   (andmap (lambda (f) (if (mine-field? f) (not (field-visible f)) (field-visible f))) lst)))

(define (chunk-list lst n)
  (let-values ([(cn ls) (split-at lst n)])
    (cons cn (if (empty? ls) empty (chunk-list ls n)))))

(define (draw-field f)
  (overlay (cond
    [(not (field-visible f))
     (square (sub1 SIZE) "solid" (make-color 50 50 255))]
    [(not (number? (field-mines f)))
     (overlay
      (circle (/ (sub1 SIZE) 2.7) "solid" "black")
      (radial-star 6 (/ (sub1 SIZE) 3) (/ (sub1 SIZE) 2) "solid" "black")
      (square (sub1 SIZE) "solid" (make-color 255 50 50)))]
    [(zero? (field-mines f))
     (square (sub1 SIZE) "solid" (make-color 200 200 255))]
    [else
     (overlay
      (text (number->string (field-mines f)) 28 "indigo")
      (square (sub1 SIZE) "solid" (make-color 150 150 255)))])
           (square SIZE "solid" "darkgray")))

(define (draw-minefield f)
  (foldl
   (λ (f a)
     (above a
            (foldl
             (lambda (f a)
               (beside a (draw-field f)))
             empty-image
             f)))
   empty-image
   (chunk-list f WIDTH)))

(define (show-mines lst)
  (overlay
   (text (if (mine-visible? lst) "Game Over!" "You Win!") 60 "red")
   (draw-minefield
    (map (lambda (f) (if (mine-field? f) (field #t (field-mines f)) f)) lst))))


(define (mouse-minefield lst mx my me)
  (if (mouse=? me "button-down")
      (enter-field (quotient mx SIZE) (quotient my SIZE) lst)
      lst))

(big-bang (random-field MINECOUT empty-fields)
          (name "Minesweeper")
          (on-mouse mouse-minefield)
          (on-draw draw-minefield)
          (stop-when game-end? show-mines))